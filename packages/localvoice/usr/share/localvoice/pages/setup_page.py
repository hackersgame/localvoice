#!/usr/bin/python3
#Triggers GPL3
#Copyright (C) 2019 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
import os
import shutil

script_path = os.path.dirname(os.path.realpath(__file__)) + "/../"


def show_setup_page(widget, Gtk, content, window):  
    #clean up odd stuff
    page_stuff = content.get_children()
    header = page_stuff[0]
    for page_thing in page_stuff:
        if page_thing != header:
            content.remove(page_thing)
    """
    header = page_stuff[0]
    mid_part = page_stuff[1]
    back_button = page_stuff[-1]
    if len(page_stuff) == 3:
        content.remove(mid_part)
        content.remove(back_button)
    """
    header.set_markup("<big><big>Setup</big></big>")

    ###########################settings page##############################
    step1_label = Gtk.Label()
    step1_label.set_markup("<big><big>Step 1:</big></big>")
    step1_label.set_halign(Gtk.Align.START)
    step1_label.set_direction(Gtk.TextDirection.LTR)
    download_deepspeech = Gtk.Button(label="Download DeepSpeech")
    download_deepspeech.connect("clicked", url_handle, Gtk, content, window, "deepspeech")
    
    step2_label = Gtk.Label()
    step2_label.set_markup("<big><big>Step 2:</big></big>")
    step2_label.set_halign(Gtk.Align.START)
    step2_label.set_direction(Gtk.TextDirection.LTR)
    download_models = Gtk.Button(label="Download models")
    download_models.connect("clicked", url_handle, Gtk, content, window, "models")
    
    step3_label = Gtk.Label()
    step3_label.set_markup("<big><big>Step 3:</big></big>")
    step3_label.set_halign(Gtk.Align.START)
    step3_label.set_direction(Gtk.TextDirection.LTR)

    ###########################Back button##############################
    back_button =  Gtk.Button(label="Use it")
    back_button.connect("clicked", display_main_page, Gtk, content, window)
    

    #select_box.pack_start(settings_grid, False, True, 0)


    #setup main window
    main_area = Gtk.Stack()
    main_area.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
    main_area.set_transition_duration(500)
    
    #content.pack_start(settings_grid, True, True, 0)
    content.pack_start(step1_label, False, False, 0)
    content.pack_start(download_deepspeech, False, False, 0)
    content.pack_start(step2_label, False, False, 0)
    content.pack_start(download_models, False, False, 0)
    content.pack_start(step3_label, False, False, 0)
    content.pack_start(back_button, False, False, 0)
    update_window(window)
    #print(dir(content))
