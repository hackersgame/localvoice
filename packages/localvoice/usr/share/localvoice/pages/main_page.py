#Triggers GPL3
#Copyright (C) 2019 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
#import os
from gi.repository.GdkPixbuf import Pixbuf, InterpType 
from gi.repository import Gdk

script_path = os.path.dirname(os.path.realpath(__file__))[:-5]
print(script_path)
print(script_path)
print(script_path)
listening = False

def display_main_page(widget, Gtk, content, window):
    #clean up odd stuff
    page_stuff = content.get_children()
    header = page_stuff[0]
    for page_thing in page_stuff:
        if page_thing != header:
            content.remove(page_thing)
    
    ###########################Main page##############################
    header.set_markup("<big><big>Welcome</big></big>")
    
    listen_button = Gtk.Button(label="Listen")
    copy_button = Gtk.Button(label="Copy")
    
    
    scroll_window = Gtk.ScrolledWindow()
    scroll_window.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
    output = Gtk.TextView()
    output.set_wrap_mode(1)
    output.set_cursor_visible(True)
    scroll_window.add(output)
    
    listen_button.connect("clicked", start_stop_listen, Gtk, content, window, output)
    copy_button.connect("clicked", copy_to_clip, Gtk, content, window, output)

    setup_button = Gtk.Button(label="Setup")
    setup_button.connect("clicked", show_setup_page, Gtk, content, window)
    
    content.pack_start(listen_button, False, False, 0)
    content.pack_start(scroll_window, True, True, 0)
    content.pack_start(copy_button, False, False, 0)
    content.pack_start(setup_button, False, False, 0)
    update_window(window)
    

def start_stop_listen(widget, Gtk, content, window, output):
    global listening
    listening = not listening
    if listening:
        widget.set_label("Stop")
        record()
    else:
        #
        stop_record()
        update_window(window)
        output_txt = voice_to_text()
        output_buffer = Gtk.TextBuffer()
        output_buffer.set_text(output_txt)
        output.set_buffer(output_buffer)
        widget.set_label("Listen")
        print("Time to process stuff...")

def copy_to_clip(widget, Gtk, content, window, output):
    txt_buff = output.get_buffer()
    txt = txt_buff.get_text(txt_buff.get_start_iter(), txt_buff.get_end_iter(), False)
    clipboard = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
    clipboard.set_text(txt,-1)
    clipboard.store()
