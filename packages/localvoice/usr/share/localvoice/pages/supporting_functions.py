#!/usr/bin/python3

#Triggers GPL3
#Copyright (C) 2019 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
import os
import operator
import subprocess
import threading
import time
import requests
from gi.repository import GLib
import shutil

deepspeech_url = "https://github.com/mozilla/DeepSpeech/releases/download/v0.7.0-alpha.0/native_client.arm64.cpu.linux.tar.xz"
models_url ="https://github.com/mozilla/DeepSpeech/releases/download/v0.6.1/deepspeech-0.6.1-models.tar.gz"
script_path = os.path.dirname(os.path.realpath(__file__))[:-5]
crap_to_cleanup = []
install_path = os.path.expanduser("~/DeepSpeech/")
try:
    os.mkdir(install_path)
except OSError:
    pass


def get_deepspeech_url():
    global deepspeech_url
    #TODO, pull the lastest URL
    return deepspeech_url

def get_models_url():
    global models_url
    return models_url

#run in the backround of download
#TODO fix resume
def _download(url, download_bar, dialog_label, file_path, resume=0):
    global install_path
    with open(file_path, "wb") as f:
        #print "Downloading %s" % file_name
        if resume == 0:
            response = requests.get(url, stream=True)
        else:
            resume_header = {'Range': 'bytes=%d-' % resume}
            response = requests.get(url, headers=resume_header, stream=True,  verify=False, allow_redirects=True)
        total_length = response.headers.get('content-length')

        if total_length is None: # no content length header
            f.write(response.content)
        else:
            dl = 0
            total_length = int(total_length)
            try:
                for data in response.iter_content(chunk_size=4096):
                    dl += len(data)
                    f.write(data)
                    progress = float(dl / total_length)
                    #print(progress)
                    GLib.idle_add(download_bar.set_fraction, progress)
            except ConnectionResetError:
                print("Warning! Download failed, restarting.")
                _download(url, download_bar, dialog_label, file_path, resume=dl)
                return
    if progress == 1:
        dialog_label.set_text("Extracting...")
        GLib.idle_add(download_bar.set_fraction, 0.0)
        try:
            shutil.unpack_archive(file_path, install_path)
        except PermissionError:
            Error = "Cannot extract to ~/DeepSpeech/\nIs it already setup?"
            dialog_label.set_text(Error)
            shutil.unpack_archive
        else:
            GLib.idle_add(download_bar.set_fraction, 1.0)
            dialog_label.set_text("Done!")
    else:
        dialog_label.set_text("Error! You'll need to try again.")

def download(widget, Gtk, content, window, url, name_txt): 
    global crap_to_cleanup
    global progress
    file_name = url.split('/')[-1]
    file_path = script_path + file_name
    crap_to_cleanup.append(file_path)
    dialog_label = Gtk.Label()
    if name_txt == "":
        dialog_label.set_markup("Please wait...")
    else:
        dialog_label.set_markup(name_txt)
    dialog = Gtk.Dialog(title="Downloading...", parent=None, flags=0, buttons=("Okay", 1))
    
    download_bar = Gtk.ProgressBar()
    download_thread = threading.Thread(target=_download, args=(url,download_bar, dialog_label, file_path))
    download_thread.daemon = True
    download_thread.start()
    
    
    dialog.get_content_area().add(dialog_label)
    dialog.get_content_area().add(download_bar)
    #dialog.get_content_area().set_size_request(360,720)
    dialog.show_all()
    response = dialog.run()
    dialog.destroy()
    return

def url_handle(widget, Gtk, content, window, goal):
    if goal == "deepspeech":
        download(widget, Gtk, content, window, get_deepspeech_url(), "")
    if goal == "models":
        download(widget, Gtk, content, window, get_models_url(), 'Scp is faster:\nDownload <a href="' + models_url + '">This</a>\nExtract to ~/DeepSpeech/')

def record():
    global install_path
    audio_file = install_path + "to_txt.wav"
    os.system('arecord -vv -fdat ' + audio_file + " &")
def stop_record():
    cmd = "killall -2 arecord"
    os.system(cmd)

def voice_to_text():
    #/deepspeech --model deepspeech-0.6.1-models/output_graph.tflite --lm deepspeech-0.6.1-models/lm.binary --trie deepspeech-0.6.1-models/trie --audio ./to_txt.wav
    cmd = []
    cmd.append(install_path + "deepspeech")
    cmd.append("--model")
    cmd.append(install_path + "deepspeech-0.6.1-models/output_graph.tflite")
    cmd.append("--lm")
    cmd.append(install_path + "deepspeech-0.6.1-models/lm.binary")
    cmd.append("--trie")
    cmd.append(install_path + "deepspeech-0.6.1-models/trie")
    cmd.append("--audio")
    cmd.append(install_path + "to_txt.wav")
    raw_outout = subprocess.check_output(cmd)
    print(raw_outout)
    return(raw_outout.decode("utf-8")) 
